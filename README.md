# Dockerize PlantUML Web Server
* [Plant UML](http://plantuml.com/)
* [Plant UML Web Server](https://github.com/plantuml/plantuml-server)
* [PlantUML & GitLab](https://docs.gitlab.com/ee/administration/integration/plantuml.html)


## How to use
```
$ docker run -d -p 8080:8080 registry.gitlab.com/graybullet/plantuml-server
```

Access to `http://{your ip address}:8080/plantuml


## LICENSE
[MIT](LICENSE)